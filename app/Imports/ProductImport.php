<?php

namespace App\Imports;

use App\Helpers\LocationHelper;
use App\Models\Category;
use App\Models\Declaration;
use App\Models\Employee;
use App\Models\Product;
use App\Models\User;
use App\Notifications\NewAccountInformation;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ProductImport implements ToCollection, WithHeadingRow
{
    public $errors = [];

    /**
     * @deprecated use DeclarationImport instead
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {

        foreach ($collection as $index => $row) {
            $row = $row->toArray();
            if (count($row) < 12) {
                continue;
            }
            $filtered = array_filter($row);
            if (count($filtered) === 0) {
                return null;
            }
            $categoryName = $row['main_category'] ?? null;
            if (!empty($categoryName)) {
                $category = Category::firstOrCreate(['name' => $categoryName, 'type' => 0]);
            } else {
                $this->errors[] = 'Row ' . ($index + 1) . ' does not have a category. Skipping';
                continue;
            }
            $typeName = $row['sub_categoryproduct'] ?? null;
            if (!empty($typeName)) {
                $row['category_id'] = Category::firstOrCreate(['name' => $typeName, 'type' => 1, 'category_id' => $category->id])->id;
            } else {
                $this->errors[] = 'Row ' . ($index + 1) . ' does not have a type. Skipping';
                continue;
            }

            $models = [];
            if (!empty($row['model_no'])) {
                $model = explode(',', $row['model_no']);
                foreach ($model as $i) {
                    $models[] = ['name' => trim($i)];
                }
            }

            $data = [
                'category_id' => $row['category_id'],
                'company' => $row['company'] ?? null,
                'brand' => $row['brand'] ?? null,
                'name' => $row['name'] ?? null,
                'model' => $models,
                'size' => $row['sizes'] ?? null,
                'country' => $row['origin'] ?? null,
                'email' => $row['email'] ?? null,
                'office_number' => $row['phone_no'] ?? null,
                'cert_no' => $row['certificate_no'] ?? null,
            ];

            if (!empty($row['id'])) {
                $product = Product::find($row['id']);
                if (!empty($product)) {
                    $product->update($data);
                } else {
                    $this->errors[] = 'Row ' . ($index + 1) . ' does not contain a valid product id. Skipping';
                }
            } else {
                Product::create($data);
            }
        }

    }
}
