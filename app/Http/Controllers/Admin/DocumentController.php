<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DocumentController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $documents = Document::query();
        $query = $request->query("q");

        if ($query) {
            $documents = $documents->where("name", "like", "%" . $query . "%");
        }

        $documents = $documents->orderBy("created_at", "desc")->paginate(10);
        return view('admin.documents.index', compact('query', 'documents'));
    }

    public function create()
    {
        return view('admin.documents.form');
    }

    public function store(Request $request)
    {
        $input = $request->input();
        $document = $request->file('document');
        $file = $document->storeAs("documents", $document->getClientOriginalName());
        $input['url'] = Storage::url($file);
        $input['user_id'] = auth()->user()->id;
        Document::create($input);

        flash()->success('Successfully created the document.');
        return redirect(route('admin.documents.index'));
    }

    public function destroy(Request $request, Document $document)
    {
        $document->delete();
        flash()->success('Successfully deleted the document.');
        return redirect(route('admin.documents.index'));
    }

}
