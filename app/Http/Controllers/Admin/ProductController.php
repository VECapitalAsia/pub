<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ProductExport;
use App\Http\Controllers\Controller;
use App\Imports\DeclarationImport;
use App\Imports\ProductImport;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $products = Product::orderBy("created_at", "desc");
        $query = $request->query("q");

        if ($query) {
            $products = $products->where("company", "like", "%" . $query . "%")->orWhere("model", "like", "%" . $query . "%")->orWhere("brand", "like", "%" . $query . "%")->orWhere("name", "like", "%" . $query . "%")->orWhereHas('category', function ($subq) use ($query) {
                $subq->where('name', 'like', '%' . $query . '%');
            });
        }

        $products = $products->paginate(10);
        return view('admin.products.index', compact('query', 'products'));
    }

    public function create()
    {
        $categories = Category::where('type', 1)->get();
        return view('admin.products.form', compact('categories'));
    }

    public function store(Request $request)
    {
        $input = $request->input();
        $models = [];
        if (!empty($input['models'])) {
            $model = explode(',', $input['models']);
            foreach ($model as $i) {
                $models[] = ['name' => trim($i)];
            }
        }
        $input['model'] = $models;
        Product::create($input);
        flash()->success('Successfully created the product.');
        return redirect(route('admin.products.index'));
    }

    public function edit(Product $product)
    {
        $categories = Category::where('type', 1)->get();
        return view('admin.products.form', compact('product', 'categories'));
    }

    public function update(Request $request, Product $product)
    {
        $input = $request->input();
        $models = [];
        if (!empty($input['models'])) {
            $model = explode(',', $input['models']);
            foreach ($model as $i) {
                $models[] = ['name' => trim($i)];
            }
        }
        $input['model'] = $models;
        $product->update($input);
        flash()->success('Successfully updated the product.');
        return redirect(route('admin.products.index'));
    }

    public function import(Request $request)
    {
        $import = new ProductImport();
        Excel::import($import, $request->import_file);
        if (!empty($import->errors)) {
            foreach ($import->errors as $error) {
                flash()->error($error);
            }
        }
        flash()->success('Successfully imported the file.');
        return back();
    }

    public function export(Request $request)
    {
        $export = new ProductExport();
        return Excel::download($export, 'products.xlsx');
    }

    public function destroy(Product $product)
    {
        $product->delete();
        flash()->success('Successfully imported the file.');
        return redirect(route('admin.products.index'));
    }

}
