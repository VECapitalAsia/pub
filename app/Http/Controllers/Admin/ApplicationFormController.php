<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ApplicationForm;
use App\Models\Category;
use App\Models\Product;
use App\Notifications\ContactForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;

class ApplicationFormController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $applications = ApplicationForm::orderBy("status", "asc")->orderBy('id', 'asc');
        $query = $request->query("q");

        if ($query) {
            $applications = $applications->where("company", "like", "%" . $query . "%");
        }
        $status = $request->query('status', 'all');
        if ($status !== 'all') {
            $applications = $applications->where('status', $status);
        }

        $applications = $applications->paginate(10);
        return view('admin.applications.index', compact('applications', 'query'));
    }

    public function show(ApplicationForm $application)
    {
        return view('admin.applications.show', compact('application'));
    }

    public function update(Request $request, ApplicationForm $application)
    {
        $input = $request->input();
        $application->update($input);
        if ($application->status == ApplicationForm::STATUS_COMPLETED) {
            //Create or update product
            if (empty($input['expiry_date'])) {
                $expiry = now()->addYears(2)->format('d-m-Y');
            } else {
                $expiry = $input['expiry_date'];
            }
            $data = [
                'company' => $application->company,
                'brand' => $application->brand,
                'model' => $application->model,
                'size' => $application->size,
                'material' => $application->type_of_materials,
                'email' => $application->email_address,
                'country' => $application->country,
                'office_number' => $application->contact_number,
                'expiry_date' => $expiry,
                'category_id' => $application->type_of_product_id ?? $application->category_id,
                'application_form_id' => $application->id,
                'cert_no' => $application->cert_no,
            ];
            if (empty($application->product)) {
                $product = Product::create($data);
                if (empty($application->cert_no)) {
                    $product->cert_no = 'WF-' . Str::padLeft($product->id, 4, '0');
                    $application->cert_no = 'WF-' . Str::padLeft($product->id, 4, '0');
                }
                $application->expiry_date = $expiry;
                if (!empty($input['cert_no'])) {
                    $application->cert_no = $input['cert_no'];
                }
                $product->save();
                $application->save();
                flash()->success('Successfully created the product.');
            } else {
                $application->product->update($data);
                flash()->success('Successfully updated the product.');
            }
        }

        flash()->success('Successfully updated the application.');
        return back();
    }

    public function destroy(ApplicationForm $application)
    {
        if ($application->product) {
            $application->product->delete();
        }
        $application->delete();
        flash()->success('Successfully deleted the application.');
        return redirect((route('admin.applications.index')));
    }

}
