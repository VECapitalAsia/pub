<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class TypeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $categories = Category::where('type', 1);
        $query = $request->query("q");

        if ($query) {
            $categories = $categories->where("name", "like", "%" . $query . "%");
        }

        $categories = $categories->orderBy("created_at", "desc")->paginate(10);
        return view('admin.types.index', compact('query', 'categories'));
    }

    public function create()
    {
        $categories = Category::where('type', 0)->get();
        return view('admin.types.form', compact('categories'));
    }

    public function store(Request $request)
    {
        $input = $request->input();
        Category::create($input);
        flash()->success('Successfully created the product type.');
        return redirect(route('admin.types.index'));
    }

    public function edit(Category $type)
    {
        $categories = Category::where('type', 0)->get();
        $category = $type;
        return view('admin.types.form', compact('category', 'categories'));
    }

    public function update(Request $request, Category $type)
    {
        $category = $type;
        $category->update($request->input());
        flash()->success('Successfully updated the product type.');
        return redirect(route('admin.types.index'));
    }

    public function destroy(Category $type)
    {
        $type->delete();
        flash()->success('Successfully deleted the type');
        return redirect(route('admin.types.index'));
    }

}
