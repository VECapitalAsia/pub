<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $categories = Category::where('type', 0);
        $query = $request->query("q");

        if ($query) {
            $categories = $categories->where("name", "like", "%" . $query . "%");
        }

        $categories = $categories->orderBy("created_at", "desc")->paginate(10);
        return view('admin.categories.index', compact('query', 'categories'));
    }

    public function create()
    {
        return view('admin.categories.form');
    }

    public function store(Request $request)
    {
        $input = $request->input();
        Category::create($input);
        flash()->success('Successfully created the category.');
        return redirect(route('admin.categories.index'));
    }

    public function edit(Category $category)
    {
        $categories = Category::where('type', 0)->get();
        return view('admin.categories.form', compact('category', 'categories'));
    }

    public function update(Request $request, Category $category)
    {
        $category->update($request->input());
        flash()->success('Successfully updated the category.');
        return redirect(route('admin.categories.index'));
    }

    public function destroy(Category $category)
    {
        $category->types()->delete();
        $category->delete();
        flash()->success('Successfully deleted the category');
        return redirect(route('admin.categories.index'));
    }

}
