<?php

namespace App\Http\Controllers;

use App\Models\ApplicationForm;
use App\Models\Category;
use App\Notifications\ContactForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function apply()
    {
        return view('apply');
    }

    public function applySubmit(Request $request)
    {
        $form = ApplicationForm::create($request->input());
        return view('submitted', compact('form'));
    }

    public function contact()
    {
        return view('contact');
    }

    public function contactSubmit(Request $request)
    {
        $input = $request->except('_token');
        $form = new ContactForm($input);
        Notification::route('mail', config('mail.mail_to'))->notify($form);

        return back()->with('message', 'Successfully sent message!');
    }

    public function admin()
    {
        $new = ApplicationForm::where('status', ApplicationForm::STATUS_NEW)->count();
        $pending = ApplicationForm::where('status', ApplicationForm::STATUS_PENDING)->count();
        $completed = ApplicationForm::where('status', ApplicationForm::STATUS_COMPLETED)->count();
        return view('admin.index', compact('new', 'pending', 'completed'));
    }

    public function products(Request $request)
    {
        $search = $request->input('search');
        if (!empty($search)) {
            $categories = Category::where('name', 'LIKE', '%' . $search . '%')->get();
        } else {
            $categories = Category::where('type', 0)->with(['categories.products'])->get();
        }
        return view('products', compact('categories'));
    }

    public function categoryShow(Request $request, Category $category)
    {
        $search = $request->input('search');
        if (!empty($search)) {
            $products = $category->products()->where(function($query) use($search) {
                $query->where('company', 'LIKE', '%' . $search . '%')
                    ->orWhere('brand', 'LIKE', '%' . $search . '%')
                    ->orWhere('model', 'LIKE', '%' . $search . '%')
                    ->orWhere('material', 'LIKE', '%' . $search . '%')
                    ->orWhere('cert_no', 'LIKE', '%' . $search . '%');
            })->get();
        } else {
            $products = $category->products;
        }
        return view('category', compact('category', 'products'));
    }
}
