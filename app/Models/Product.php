<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name', 'company', 'brand', 'model', 'size', 'material', 'country', 'email', 'office_number',
        'expiry_date', 'category_id', 'application_form_id', 'cert_no',
    ];

    protected $casts = [
        'model' => 'array'
    ];

    public function setExpiryDateAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['expiry_date'] = Carbon::createFromFormat('d-m-Y', $value)->format('Y-m-d');
        } else {
            $this->attributes['expiry_date'] = null;
        }
    }

    public function getExpiryDateAttribute($value)
    {
        if (!empty($value)) {
            return Carbon::parse($value)->format('d-m-Y');
        }
        return null;
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function application()
    {
        return $this->belongsTo(ApplicationForm::class);
    }
}
