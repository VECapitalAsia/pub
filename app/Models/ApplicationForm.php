<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ApplicationForm extends Model
{

    const STATUS_NEW = 0;
    const STATUS_PENDING = 1;
    const STATUS_COMPLETED = 2;
    const STATUS_REJECTED = 10;

    const STATUS = [
        self::STATUS_NEW => 'New',
        self::STATUS_PENDING => 'Pending',
        self::STATUS_COMPLETED => 'Completed',
        self::STATUS_REJECTED => 'Rejected',
    ];

    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name', 'uen', 'office_address', 'office_email', 'contact_person', 'designation',
        'email_address', 'contact_number', 'category_id', 'type_of_product_id', 'type_of_materials',
        'country', 'brand', 'model', 'size', 'manufacturer', 'last_tested_batch', 'remarks', 'date',
        'name_submitted', 'designation_submitted', 'cert_no', 'company', 'expiry_date', 'status',
        'internal_remarks',
    ];

    protected $casts = [
        'model' => 'array'
    ];

    public function setDateAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['date'] = Carbon::createFromFormat('d-m-Y', $value)->format('Y-m-d');
        } else {
            $this->attributes['date'] = null;
        }
    }

    public function getDateAttribute($value)
    {
        if (!empty($value)) {
            return Carbon::parse($value)->format('d-m-Y');
        }
        return null;
    }

    public function setExpiryDateAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['expiry_date'] = Carbon::createFromFormat('d-m-Y', $value)->format('Y-m-d');
        } else {
            $this->attributes['expiry_date'] = null;
        }
    }

    public function getExpiryDateAttribute($value)
    {
        if (!empty($value)) {
            return Carbon::parse($value)->format('d-m-Y');
        }
        return null;
    }

    public function setLastTestedBatchAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['last_tested_batch'] = Carbon::createFromFormat('d-m-Y', $value)->format('Y-m-d');
        } else {
            $this->attributes['last_tested_batch'] = null;
        }
    }

    public function getLastTestedBatchAttribute($value)
    {
        if (!empty($value)) {
            return Carbon::parse($value)->format('d-m-Y');
        }
        return null;
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function type()
    {
        return $this->belongsTo(Category::class, 'type_of_product_id');
    }

    public function product()
    {
        return $this->hasOne(Product::class);
    }
}
