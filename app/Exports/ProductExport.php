<?php

namespace App\Exports;
use App\Models\Product;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;


class ProductExport implements FromCollection, WithMapping,ShouldAutoSize,WithHeadings
{

    public function collection()
    {
        return Product::all();
    }

    public function headings(): array
    {
        return [
            'ID', 'Main Category', 'Sub category(Product)', 'Name', 'Company', 'Certificate No.', 'Brand', 'Model No.', 'Sizes',  'Origin', 'Email', 'Phone No'
        ];
    }

    public function map($row): array
    {
        return [
            $row->id,
            $row->category ? ($row->category->category ? $row->category->category->name : '') : '',
            $row->category ? $row->category->name : '',
            $row->name,
            $row->company,
            $row->cert_no,
            $row->brand,
            implode(', ', collect($row->model ?? [])->pluck('name')->toArray()),
            $row->size,
            $row->country,
            $row->email,
            $row->office_number,
        ];
    }

}
