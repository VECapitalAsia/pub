<?php

namespace App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Str;

class ContactForm extends Notification
{
    protected $input;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        $this->input = $input;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if (empty($this->input['contact_type'])) {
            $mail = (new MailMessage)
                ->subject('[' . config('app.name') . '] Contact Form')
                ->greeting('There is a new contact form submission!');
        } else {
            $mail = (new MailMessage)
                ->subject('[' . config('app.name') . '] Digital Maturity Assessment')
                ->greeting('There is a new DMA form submission!');
        }
        unset($this->input['contact_type']);
        foreach ($this->input as $key => $value) {
            if (is_array($value)) {
                $value = implode(', ', $value);
            }
            $mail->line(ucwords(str_replace('_' , ' ', Str::snake($key))) . ': ' . $value);
        }
        return $mail;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
