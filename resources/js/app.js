require('./bootstrap');


import Vue from "vue";
import DatePicker from "vue2-datepicker";

Vue.component("datepicker", DatePicker);
window.Vue = Vue;

$(() => {
    $("#menu-toggle").on("click", () => {
        $("#wrapper").toggleClass("toggled");
    });
    //when sidebar is collapsed, clicking on a sidebar icon will expand sidebar and submenu items
    //if sidebar is expanded, clicking on menu toggle will collapse submenu items
    if ($("#wrapper.toggled")[0]) {
        $(".sidebar-toggle").on("click", () => {
            $("#wrapper").removeClass("toggled");
            $("#menu-toggle").on("click", () => {
                $(".sidebar-submenu").removeClass("show");
            });
        });
    }
});

