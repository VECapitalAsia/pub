@extends('layouts.app')

@section('content')
    <div class="container-fluid p-5" style="min-height: calc(100vh - 495px);">
        <div class="row">
            <div class="col-12">
                <h4 style="color: #707070" class="font-weight-bolder text-black pb-5 mt-4">Registered Product Listing</h4>
                <form method="GET">
                    <input type="text" name="search" class="form-control" value="{{ request('search', '') }}" placeholder="Search.." />
                </form>
                <small class="text-muted">Search by Company Name, Certificate Number, Product Type, Brand Name, Model</small>
            </div>
            <div class="col-12 mt-5">
                <a href="{{ route('products') }}" class="text-decoration-none text-blue"><i class="fa fa-arrow-left mr-3"></i> Back to Registered Product Listing</a>
            </div>
        </div>
        <div class="mt-5 col-12 pb-5">
            <div class="row">
                <div class="d-none d-md-block col-md-3">
                    @if ($category->category)
                        <h4 class="text-blue font-weight-bolder mb-5">{{ $category->category->name }}</h4>
                        @foreach ($category->category->categories as $type)
                            <a href="{{ route('categories.products', [$type->getRouteKey()]) }}" class="mb-3 text-black-50 text-decoration-none d-block {{ $type->id == $category->id ? 'font-weight-bold' : '' }}">{{ $type->name }}</a>
                        @endforeach
                    @endif
                </div>
                <div class="col-md-9">
                    <h5 class="text-blue">{{ $category->name }}</h5>
                    <div class="table-responsive">
                        <table class="table table-rounded table-striped w-100 mt-4">
                            <thead>
                            <tr>
                                <th>Company</th>
                                <th>Name</th>
                                <th>Brand</th>
                                <th>Model</th>
                                <th>Size</th>
                                <th>Origin</th>
                                <th>Certificate No</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($products as $product)
                                <tr>
                                    <td>{{ $product->company }}
                                        @if (!empty($product->email))
                                            <br /><small><a href="mailto:{{ $product->email }}">{{ $product->email }}</a></small>
                                        @endif
                                        @if (!empty($product->office_number))
                                            <br /><small>Tel: <a href="tel:{{ $product->office_number }}">{{ $product->office_number }}</a></small>
                                        @endif
                                    </td>
                                    <td>{{ $product->name }}</td>
                                    <td>{{ $product->brand }}</td>
                                    <td>{{ implode(', ', collect($product->model ?? [])->pluck('name')->toArray()) }}</td>
                                    <td>{{ $product->size }}</td>
                                    <td>{{ $product->country }}</td>
                                    <td>{{ $product->cert_no }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="8" class="text-center bg-white">There are no products here.</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
