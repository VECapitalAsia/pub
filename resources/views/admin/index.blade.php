<x-app-layout>
    <div class="container-fluid" id="app">

        @if(session()->has('message'))
            <div class="alert alert-danger">
                {{ session()->get('message') }}
            </div>
        @endif

        {{-- header --}}
        <div class="border-bottom pb-4">
            <h5><i class="fa fa-home"></i> Dashboard</h5>
        </div>

        <div class="row">
            <div class="col-lg-4 p-5">
                <div class="card shadow-lg border-0 mb-5">
                    <div class="card-body p-2 p-lg-5">
                        <h5>Total Applicant</h5>
                        <h1 style="font-size: 60px" class="text-success">{{ \App\Models\ApplicationForm::count() }}</h1>
                        <div class="text-right" style="margin-top: -50px;">
                            <img src="/images/total_applicant.svg" class="mw-100" />
                        </div>
                    </div>
                </div>
                <div class="card shadow-lg border-0 mb-5">
                    <div class="card-body p-2 p-lg-5">
                        <h5>Total Pending</h5>
                        <h1 style="font-size: 60px" class="text-warning">{{ $pending }}</h1>
                        <div class="text-right" style="margin-top: -50px;">
                            <img src="/images/total_pending.svg" class="mw-100" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 p-5">
                <div class="card shadow-lg border-0 mb-5">
                    <div class="card-body p-2 p-lg-5">
                        <h5>Total New Applicant</h5>
                        <h1 style="font-size: 60px" class="text-danger">{{ $new }}</h1>
                        <div class="text-right" style="margin-top: -50px;">
                            <img src="/images/total_new.svg" class="mw-100" />
                        </div>
                    </div>
                </div>
                <div class="card shadow-lg border-0 mb-5">
                    <div class="card-body p-2 p-lg-5">
                        <h5>Total Completed</h5>
                        <h1 style="font-size: 60px" class="text-blue">{{ $completed }}</h1>
                        <div class="text-right" style="margin-top: -50px;">
                            <img src="/images/total_complete.svg" class="mw-100" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 p-5">
                <div class="card shadow-lg border-0 mb-5">
                    <div class="card-body p-2 p-lg-5">
                        <h5>Application Management</h5>
                        <div class="my-5 text-center">
                            <canvas id="myChart" width="400px" class="mw-100 mx-auto"></canvas>
                        </div>
                        <div class="text-center">

                            <img src="/images/applicant.svg" class="mw-100" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @section('footer')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.5.1/chart.min.js" integrity="sha512-Wt1bJGtlnMtGP0dqNFH1xlkLBNpEodaiQ8ZN5JLA5wpc1sUlk/O5uuOMNgvzddzkpvZ9GLyYNa8w2s7rqiTk5Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script type="text/javascript">
            var ctx = document.getElementById("myChart");
            var myChart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    labels: ['New Applicant', 'Pending', 'Completed'],
                    datasets: [{
                        label: '# of appplicants',
                        data: [{{ $new }}, {{ $pending }}, {{ $completed }}],
                        backgroundColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(33,49,173, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    //cutoutPercentage: 40,
                    responsive: false,

                }
            });
        </script>
    @endsection
</x-app-layout>
