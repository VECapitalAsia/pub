<nav class="navbar navbar-expand-lg navbar-light bg-light navbar-admin-site">
    <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse d-flex flex-row-reverse" id="navbarSupportedContent">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-expanded="false" style="color: black">
                {{ Auth::user()->name }}
            </a>
            <ul class="dropdown-menu dropdown-menu-right mr-5" aria-labelledby="navbarDropdown">
              <li class="dropdown-item"><a href="{{ route('logout') }}" class="text-left pl-3" style="font-size: 18px;">Log Out</a></li>
            </ul>
        </div>
    </div>
</nav>
