<nav id="sidebar" class="active">
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li class="sidebar-brand d-flex">
                <a href="#" id="menu-toggle"><i class="fa fa-bars" style="margin-right: 50px" aria-hidden="true"></i></a>
            </li>
            <li class="mt-4">
                <a href="{{ route('admin.dashboard') }}"><i class="fas fa-home" aria-hidden="true" style="margin-right: 50px"></i><span>Dashboard</span></a>
            </li>
            <li>
                <a href="#inventory" class="sidebar-toggle" data-toggle="collapse" aria-expanded="false"><i class="fas fa-shopping-cart" aria-hidden="true" style="margin-right: 50px"></i><span>Products</span></a>
                <div id="inventory" class="collapse sidebar-submenu">
                    <a href="{{ route('admin.products.index') }}">
                        <span class="menu-collapsed">Products</span>
                    </a>
                    <a href="{{ route('admin.categories.index') }}">
                        <span class="menu-collapsed">Categories</span>
                    </a>
                    <a href="{{ route('admin.types.index') }}">
                        <span class="menu-collapsed">Types</span>
                    </a>
                </div>
            </li>
            <li>
                <a href="{{ route('admin.applications.index') }}"><i class="fas fa-file-alt" aria-hidden="true" style="margin-right: 50px"></i><span>Application Management</span></a>
            </li>
            <li>
                <a href="{{ route('admin.documents.index') }}"><i class="fas fa-folder" aria-hidden="true" style="margin-right: 50px"></i><span>File Management</span></a>
            </li>
        </ul>
    </div>
    <!-- /#sidebar-wrapper -->
</nav>
