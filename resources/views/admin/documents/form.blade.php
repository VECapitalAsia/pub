<x-app-layout>
    <div class="container-fluid" id="app">
        {{-- header --}}
        <div class="d-flex justify-content-between p-3" style="border-bottom-width:2px">
            <div>
                <h4 class="font-weight-bold">Upload File</h4>
            </div>
            <div class="mt-3 mb-3" style="border-top-width:3px">
                <div>
                    <a href="{{ route('admin.documents.index') }}" class="btn btn-alt px-5">Back</a>
                </div>
            </div>
        </div>

        {{--Form --}}
        <div class="card shadow bg-white mt-4">
            <form action="{{ route('admin.documents.store') }}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="card-header bg-theme rounded-top">
                    <div class="ms-4 mt-2 mb-2">File Information</div>
                </div>

                <div class="card-body">
                    <div class="container mt-5">
                        <div class="form-group row mb-5 mt-5">
                            <label class="col-md-5 text-right col-form-label">Name</label>
                            <div class="col-md-5">
                                <input type="text" name="name" class="form-control" required />
                                @if($errors->has('name'))
                                <div class="text-danger">{{ $errors->first('name') }}</div>
                                @endif
                            </div>
                        </div>
                        <input type="hidden" name="type" value="0" />
                        <div class="form-group row mb-5 mt-5">
                            <label class="col-md-5 text-right col-form-label">File</label>
                            <div class="col-md-5">
                                <input type="file" name="document" class="form-control" required />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center mt-5 mb-4">
                    <button class="btn btn-primary px-5">Upload</button>
                </div>
            </form>
        </div>
    </div>

</x-app-layout>

