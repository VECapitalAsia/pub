<x-app-layout>
    <div class="container-fluid" id="app">

        {{-- header --}}
        <div class="row justify-content-between align-items-center w-100 border-bottom pb-4">
            <div class="col-6">
                <h5>Document List</h5>
            </div>
            <div class="col-6 text-right">
                <a href="{{ route('admin.documents.create') }}" class="btn btn-identity px-5">Upload</a>
            </div>
        </div>
        {{-- search and button --}}
        <div class="row my-4">
            <div class="col-lg-4 col-md-6">
                <form id="frmSearch" action="{{ route('admin.documents.index') }}" method="GET">
                    <div class="w-100">
                        <input type="text" name="q" placeholder="Search in the current filter" value="{{ $query }}" class="form-control d-inline-block" style="width: calc(100% - 100px); background: #ececec">
                        <i class="fa fa-search" onclick="document.getElementById('frmSearch').submit()" style="cursor: pointer;"></i>
                    </div>
                </form>
            </div>
        </div>

        {{-- index table --}}
        <div class="row mt-2">
            <div class="table-responsive">
                <table class="table table-rounded table-striped w-100 mt-4">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Last Updated</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($documents as $document)
                            <tr>
                                <td>{{ $document->name }}</td>
                                <td>{{ $document->updated_at->format('h:ia d/m/Y') }}</td>
                                <td class="d-flex">
                                    <a target="_blank" class="btn btn-blue text-white" href="{{ $document->url }}">View</a>
                                    <form action="{{ route('admin.documents.destroy', [$document->getRouteKey()]) }}" method="POST" class="d-inline-block ml-4">
                                        @method('DELETE')
                                        @csrf
                                        <div class="text-right">
                                            <button class="btn" type="submit" onclick="return confirm('Are you sure you want to delete? You cannot revert this.')">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3" class="text-center bg-white">There are no documents.</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        {{ $documents->links() }}
    </div>
</x-app-layout>
