<x-app-layout>
    <div class="container-fluid" id="app">
        {{-- header --}}
        <div class="d-flex justify-content-between p-3" style="border-bottom-width:2px">
            <div>
                <h4 class="font-weight-bold"> @if (!isset($product))Create @else Edit @endif Product</h4>
                <h6>@if (!isset($product))Create @else Edit @endif a new product</h6>
            </div>
            <div class="mt-3 mb-3" style="border-top-width:3px">
                <div>
                    @if (!empty($product))
                    <form action="{{ route('admin.products.destroy', [$product->getRouteKey()]) }}" method="POST" class="d-inline-block mr-3">
                        @method('DELETE')
                        @csrf
                        <div class="text-right">
                            <button class="btn btn-danger px-4" type="submit" onclick="return confirm('Are you sure you want to delete? This will delete all the types related to this product. You cannot revert this.')">
                                Delete
                            </button>
                        </div>
                    </form>
                    @endif
                    <a href="{{ route('admin.products.index') }}" class="btn btn-alt px-5">Back</a>
                </div>
            </div>
        </div>

        {{--Form --}}
        <div class="card shadow bg-white mt-4">
        @if(isset($product))
            <form action="{{ route('admin.products.update', $product->id) }}" method="POST" enctype="multipart/form-data">
            @method('PATCH')
        @else
            <form action="{{ route('admin.products.store') }}" method="POST" enctype="multipart/form-data">
        @endif

            @csrf

            @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
            @endif

                <div class="card-header bg-theme rounded-top">
                    <div class="ms-4 mt-2 mb-2">Product Information</div>
                </div>

                <div class="card-body">
                    <div class="container mt-4">
                        <div class="form-group row mb-4 mt-4">
                            <label class="col-md-4 text-right col-form-label">Type</label>
                            <div class="col-md-8">
                                <select name="category_id" class="form-control" required>
                                    @foreach ($categories as $categoryTwo)
                                        <option value="{{ $categoryTwo->id }}" {{ !empty($product) ? ($product->category_id == $categoryTwo->id ? 'selected' : '') : '' }}>{{ $categoryTwo->name }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('category_id'))
                                    <div class="text-danger">{{ $errors->first('category_id') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-4 mt-4">
                            <label class="col-md-4 text-right col-form-label">Company</label>
                            <div class="col-md-8">
                                <input type="text" name="company" value="{{ !empty(old()) ? old('company') : (isset($product) ? $product->company : '') }}" class="form-control" required />
                                @if($errors->has('company'))
                                    <div class="text-danger">{{ $errors->first('company') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-4 mt-4">
                            <label class="col-md-4 text-right col-form-label">Name</label>
                            <div class="col-md-8">
                                <input type="text" name="name" value="{{ !empty(old()) ? old('name') : (isset($product) ? $product->name : '') }}" class="form-control" required />
                                @if($errors->has('name'))
                                    <div class="text-danger">{{ $errors->first('name') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-4 mt-4">
                            <label class="col-md-4 text-right col-form-label">Certificate No</label>
                            <div class="col-md-8">
                                <input type="text" name="cert_no" value="{{ !empty(old()) ? old('cert_no') : (isset($product) ? $product->cert_no : '') }}" class="form-control" required />
                                @if($errors->has('cert_no'))
                                    <div class="text-danger">{{ $errors->first('cert_no') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-4 mt-4">
                            <label class="col-md-4 text-right col-form-label">Brand</label>
                            <div class="col-md-8">
                                <input type="text" name="brand" value="{{ !empty(old()) ? old('brand') : (isset($product) ? $product->brand : '') }}" class="form-control" required />
                                @if($errors->has('brand'))
                                    <div class="text-danger">{{ $errors->first('brand') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-4 mt-4">
                            <label class="col-md-4 text-right col-form-label">Country</label>
                            <div class="col-md-8">
                                <input type="text" name="country" value="{{ !empty(old()) ? old('country') : (isset($product) ? $product->country : '') }}" class="form-control" required />
                                @if($errors->has('country'))
                                    <div class="text-danger">{{ $errors->first('country') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-4 mt-4">
                            <label class="col-md-4 text-right col-form-label">Size</label>
                            <div class="col-md-8">
                                <input type="text" name="size" value="{{ !empty(old()) ? old('size') : (isset($product) ? $product->size : '') }}" class="form-control" />
                                @if($errors->has('size'))
                                    <div class="text-danger">{{ $errors->first('size') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-4 mt-4">
                            <label class="col-md-4 text-right col-form-label">Model</label>
                            <div class="col-md-8">
                                <input type="text" name="models" value="{{ implode(', ', collect($product->model ?? [])->pluck('name')->toArray()) }}" class="form-control" />
                                @if($errors->has('size'))
                                    <div class="text-danger">{{ $errors->first('size') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-4 mt-4">
                            <label class="col-md-4 text-right col-form-label">Email</label>
                            <div class="col-md-8">
                                <input type="email" name="email" value="{{ !empty(old()) ? old('email') : (isset($product) ? $product->email : '') }}" class="form-control" required />
                                @if($errors->has('email'))
                                    <div class="text-danger">{{ $errors->first('email') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-4 mt-4">
                            <label class="col-md-4 text-right col-form-label">Phone No</label>
                            <div class="col-md-8">
                                <input type="text" name="office_number" value="{{ !empty(old()) ? old('office_number') : (isset($product) ? $product->office_number : '') }}" class="form-control" required />
                                @if($errors->has('office_number'))
                                    <div class="text-danger">{{ $errors->first('office_number') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center mt-4 mb-5">
                    <button class="btn btn-primary px-5">{{ isset($product) ? 'Update' : 'Create' }}</button>
                </div>
            </form>
        </div>
    </div>

</x-app-layout>
