<x-app-layout>
    <div class="container-fluid" id="app">

        @if(session()->has('message'))
        <div class="alert alert-danger">
            {{ session()->get('message') }}
        </div>
        @endif

        {{-- header --}}
        <div class="row justify-content-between align-items-center w-100 border-bottom pb-4">
            <div class="col-6">
                <h5>Products List</h5>
            </div>
            <div class="col-6 text-right">
                <form method="POST" action="{{ route('admin.products.import') }}" enctype="multipart/form-data">
                    @csrf
                    <input name="import_file" type="file" accept=".xlsx" style="display: none" id="file-upload" onchange="this.form.submit()" />
                </form>
                <button class="btn btn-primary px-5 me-3" type="button" onclick="$('#file-upload').click()">Import</button>
                <a class="btn btn-secondary text-white px-5" href="{{ route('admin.products.export') }}" data-bs-toggle="modal">Export</a>
                <a href="{{ route('admin.products.create') }}" class="btn btn-identity px-5">Create</a>
            </div>
        </div>
        {{-- search and button --}}
        <div class="row my-4">
            <div class="col-lg-4 col-md-6">
                <form id="frmSearch" action="{{ route('admin.products.index') }}" method="GET">
                    <div class="w-100">
                        <input type="text" name="q" placeholder="Search in the current filter" value="{{ $query }}" class="form-control d-inline-block" style="width: calc(100% - 100px); background: #ececec">
                        <i class="fa fa-search" onclick="document.getElementById('frmSearch').submit()" style="cursor: pointer;"></i>
                    </div>
                </form>
            </div>
        </div>

        {{-- index table --}}
        <div class="row mt-2">
            <div class="table-responsive">
                <table class="table table-rounded table-striped w-100 mt-4">
                    <thead>
                        <tr>
                            <th>Type</th>
                            <th>Company</th>
                            <th>Name</th>
                            <th>Brand</th>
                            <th>Model</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($products as $product)
                            <tr>
                                <td>{{ $product->category ? $product->category->name : '-' }}</td>
                                <td>{{ $product->company }}</td>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->brand }}</td>
                                <td>{{ implode(', ', collect($product->model ?? [])->pluck('name')->toArray()) }}</td>
                                <td class="d-flex">
                                    <a class="btn-link btn-sm" href="{{ route('admin.products.edit', [$product->getRouteKey()]) }}">Edit</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6" class="text-center bg-white">There are no products.</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        {{ $products->links() }}
    </div>
</x-app-layout>
