<x-app-layout>
    <div class="container-fluid" id="app">

        {{-- header --}}
        <div class="row justify-content-between align-items-center w-100 border-bottom pb-4">
            <div class="col-6">
                <span>
                    <h5>Application Management</h5>
                </span>
            </div>
        </div>
        {{-- search and button --}}
        <div class="row my-4">
            <div class="col-lg-4 col-md-6">
                <form id="frmSearch" action="{{ route('admin.applications.index') }}" method="GET">
                    <div class="row">
                        <div class="col-md-5">
                            <select name="status" class="form-control" onchange="this.form.submit()">
                                <option value="all" {{ request('status', 'all') == 'all' ? 'selected' : '' }}>All</option>
                                @foreach (\App\Models\ApplicationForm::STATUS as $key => $value)
                                    <option value="{{ $key }}" {{ request('status', 'all') == strval($key) ? 'selected' : '' }}>{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-7 mt-3 mt-md-0">
                            <input type="text" name="q" placeholder="Search in the current filter" value="{{ $query }}" class="form-control d-inline-block" style="width: calc(100% - 100px); background: #ececec">

                            <i class="fa fa-search" onclick="document.getElementById('frmSearch').submit()" style="cursor: pointer;"></i>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        {{-- index table --}}
        <div class="row mt-2">
            <div class="table-responsive">
                <table class="table table-rounded table-striped w-100 mt-4">
                    <thead>
                        <tr>
                            <th>Application Number</th>
                            <th>Company Name</th>
                            <th>Email Address</th>
                            <th>Contact Number</th>
                            <th>Cert Number</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($applications as $application)
                            <tr>
                                <td>{{ $application->id }}</td>
                                <td>{{ $application->company }}</td>
                                <td>{{ $application->email_address }}</td>
                                <td>{{ $application->contact_number }}</td>
                                <td>-</td>
                                <td>
                                    @if ($application->status == \App\Models\ApplicationForm::STATUS_NEW)
                                        <span class="badge badge-success">NEW</span>
                                    @elseif ($application->status == \App\Models\ApplicationForm::STATUS_PENDING)
                                        <span class="badge badge-warning">PENDING</span>
                                    @elseif ($application->status == \App\Models\ApplicationForm::STATUS_COMPLETED)
                                        <span class="badge badge-primary">COMPLETED</span>
                                    @elseif ($application->status == \App\Models\ApplicationForm::STATUS_REJECTED)
                                        <span class="badge badge-danger">REJECTED</span>
                                    @endif
                                    <br />
                                    <small><a href="#application-{{ $application->id }}" data-toggle="collapse">Remarks</a></small>
                                </td>
                                <td class="d-flex">
                                    <a class="btn-link btn-sm" href="{{ route('admin.applications.show', [$application->getRouteKey()]) }}">View</a>
                                </td>
                            </tr>
                            <tr id="application-{{ $application->id }}" class="collapse">
                                <td colspan="7">
                                    <div class="container">
                                        <form action="{{ route('admin.applications.update', [$application->getRouteKey()]) }}" method="POST">
                                            <textarea class="form-control mb-3" name="internal_remarks">{{ $application->internal_remarks }}</textarea>
                                            @method('PUT')
                                            @csrf
                                            <button type="submit" class="btn btn-success" onclick="return confirm('Are you sure you want to update this? You cannot revert this.')">
                                                Update
                                            </button>
                                            <button type="button" data-target="#application-{{ $application->id }}" data-toggle="collapse" class="btn btn-outline-info">Close</button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="7" class="text-center bg-white">There are no applications.</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        {{ $applications->links() }}
    </div>
</x-app-layout>
