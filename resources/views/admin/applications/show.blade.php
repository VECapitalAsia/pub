<x-app-layout>
    <div class="container-fluid" id="app">

        {{-- header --}}
        <div class="row my-5 pb-5 border-bottom border-gray">
            <div class="col-lg-5">
                <h5><i class="fa fa-file-alt"></i> Application Form</h5>
            </div>
            <div class="col-lg-7 text-right">

                <form action="{{ route('admin.applications.destroy', [$application->getRouteKey()]) }}" v-if="edit" method="POST" class="d-inline-block ml-4">
                    @method('DELETE')
                    @csrf
                    <div class="text-right">
                        <button type="submit" onclick="return confirm('Are you sure you want to delete? You cannot revert this.')" class="btn btn-danger btn-big mr-3">Delete</button>
                    </div>
                </form>
                <button type="button" @click="toggleEdit()" class="btn btn-warning btn-big mr-3">@{{ edit ? 'Cancel Edit' : 'Edit' }}</button>
                <a href="{{ route('admin.applications.index') }}" class="btn btn-blue btn-big text-white">Back</a>
            </div>
        </div>
        <div class="container-fluid px-5">

            <form id="application-form" method="POST" action="{{ route('admin.applications.update', [$application->getRouteKey()]) }}">
                @csrf
                @method('PUT')
                <h4 class="mb-4 pb-4 border-bottom border-black font-weight-bold">Application Form</h4>
                <div class="row mb-5 pb-5">
                    <div class="col-lg-6">
                        <label class="text-black mt-5">1. Applicant (Company Name)</label>
                        <input name="company" value="{{ $application->company }}" type="text" class="form-control py-3" :disabled="!edit">

                        <label class="text-black mt-5">2. Unique Entity Number (UEN)</label>
                        <input name="uen" value="{{ $application->uen }}" type="text" class="form-control py-3" :disabled="!edit">

                        <label class="text-black mt-5">3. Office Address</label>
                        <input name="office_address" value="{{ $application->office_address }}" type="text" class="form-control py-3" :disabled="!edit">

                        <label class="text-black mt-5">4. Office Email Address</label>
                        <input name="office_email" value="{{ $application->office_email }}" type="text" class="form-control py-3" :disabled="!edit">

                    </div>
                    <div class="col-lg-6">
                        <label class="text-black mt-5">5. Contact Person</label>
                        <input name="contact_person" value="{{ $application->contact_person }}" type="text" class="form-control py-3" :disabled="!edit">

                        <label class="text-black mt-5">6. Designation</label>
                        <input name="designation" value="{{ $application->designation }}" type="text" class="form-control py-3" :disabled="!edit">

                        <label class="text-black mt-5">7. Email Address</label>
                        <input name="email_address" value="{{ $application->email_address }}" type="text" class="form-control py-3" :disabled="!edit">

                        <label class="text-black mt-5">8. Contact Number</label>
                        <input name="contact_number" value="{{ $application->contact_number }}" type="text" class="form-control py-3" :disabled="!edit">
                    </div>
                </div>
                <h4 class="my-5 pb-5 border-bottom border-black font-weight-bold">Product Information</h4>
                <div class="row mb-5 pb-5">
                    <div class="col-lg-6">
                        <label class="text-black mt-5">1. Type of Category</label>
                        <select name="category_id" v-model="category" @change="selectCategory()" class="form-control" :disabled="!edit">
                            <option v-for="category in categories" :value="category.id">@{{ category.name }}</option>
                        </select>

                        <label class="text-black mt-5">2. Type of Products</label>
                        <template v-if="selected_category">
                            <select name="type_of_product_id" class="form-control" :required="selected_category.types.length > 0" :disabled="!edit">
                                <option v-for="type in selected_category.types" :value="type.id">@{{ type.name }}</option>
                            </select>
                        </template>
                        <input type="text" v-if="!selected_category" class="form-control" value="Please select a category first" disabled />

                        <label class="text-black mt-5">3. Brand</label>
                        <input name="brand" value="{{ $application->brand }}" type="text" class="form-control py-3" :disabled="!edit">

                        <label class="text-black mt-5">4. Model</label>
                        <template v-for="(model, index) in models">
                            <input :name="'model[' + index + '][name]'" v-model="model.name" type="text" class="form-control py-3 mb-1" :disabled="!edit">
                            <template v-if="edit">
                                <small @click="models.splice(index, 1)" class="cursor-pointer">Remove</small><br />
                            </template>
                        </template>
                        <template v-if="edit">
                            <small @click="models.push({name: ''})" class="cursor-pointer"><i class="fa fa-plus"></i> Add</small><br />
                        </template>

                        <label class="text-black mt-5">5. Product Size</label>
                        <input name="size" value="{{ $application->size }}" type="text" class="form-control py-3" :disabled="!edit">

                        <label class="text-black mt-5">6. Cert No</label>
                        <input name="cert_no" value="{{ $application->cert_no }}" type="text" class="form-control py-3" :disabled="!edit">

                    </div>
                    <div class="col-lg-6">
                        <label class="text-black mt-5">7. Type of Materials</label>
                        <input name="type_of_materials" value="{{ $application->type_of_materials }}" type="text" class="form-control py-3" :disabled="!edit">

                        <label class="text-black mt-5">8. Country of Manufacture</label>
                        <input name="country" value="{{ $application->country }}" type="text" class="form-control py-3" :disabled="!edit">

                        <label class="text-black mt-5">9. Name of Manufacturer</label>
                        <input name="manufacturer" value="{{ $application->manufacturer }}" type="text" class="form-control py-3" :disabled="!edit">

                        <label class="text-black mt-5">10. Date of last tested batch</label>
                        <div class="w-100">
                            <datepicker class="w-100" format="DD-MM-YYYY" :value="last_tested_batch" :input-attr="{ name: 'last_tested_batch'}" name="last_tested_batch" v-model="last_tested_batch" :disabled="!edit"></datepicker>
                        </div>

                        <label class="text-black mt-5">11. Remarks</label>
                        <textarea name="remarks" type="text" class="form-control py-3" :disabled="!edit">{{ $application->remarks }}</textarea>

                        <label class="text-black mt-5">12. Expiry Date</label>
                        <div class="w-100">
                            <datepicker class="w-100" format="DD-MM-YYYY" name="expiry_date" :input-attr="{ name: 'expiry_date'}" v-model="expiry_date" :disabled="!edit"></datepicker>
                        </div>

                    </div>
                </div>
                <h4 class="my-5 pb-5 border-bottom border-black font-weight-bold">Fees - 2 years</h4>
                $480.00 (per product) for Verification & Registration Fee<br /><br />
                Payment Method
                <ul class="mt-4 mb-5">
                    <li>Bank Transfer</li>
                    <li>Pay Now</li>
                </ul>
                <h4 class="my-5 pb-5 border-bottom border-black font-weight-bold">Declaration</h4>
                I declare that:
                <div class="mt-3 pl-3">
                    <label><input type="checkbox" checked disabled /> I have read and understood the Compliance Notes</label>
                </div>
                <div class="mt-3 pl-3">
                    <label><input type="checkbox" checked disabled /> I confirm the certificate will be issued to the stated company’s name.</label>
                </div>
                <div class="mt-3 pl-3">
                    <label><input type="checkbox" checked disabled /> All product information are accurate.</label>
                </div>
                <div class="mt-3 pl-3">
                    <label><input type="checkbox" checked disabled /> I am the authorized person by the company to submit this application form.</label>
                </div>
                <div class="mt-3 pl-3">
                    <label><input type="checkbox" checked disabled /> PDPA : I consent and understand that details submitted will be shared with PUB.</label>
                </div>
                <div class="row pb-5 mb-5">
                    <div class="col-lg-6">
                        <label class="text-black mt-5">Name</label>
                        <input name="name_submitted" value="{{ $application->name_submitted }}" type="text" class="form-control py-3" :disabled="!edit">

                        <label class="text-black mt-5">Designation</label>
                        <input name="designation_submitted" value="{{ $application->designation_submitted }}" type="text" class="form-control py-3" :disabled="!edit">

                        <label class="text-black mt-5">Date</label>
                        <div class="w-100">
                            <datepicker class="w-100" format="DD-MM-YYYY" :value="date" :input-attr="{ name: 'date'}" name="date" v-model="date" :disabled="!edit"></datepicker>
                        </div>
                    </div>
                </div>
                <div v-if="edit" class="pb-5 text-center">
                    <button type="submit" class="btn btn-big btn-blue text-white">Update</button>
                </div>

            </form>
                <h4 class="my-5 pb-5 border-bottom border-black font-weight-bold">Approval Status</h4>
                <div class="mb-5 pb-5">
                    @if ($application->status == \App\Models\ApplicationForm::STATUS_NEW)
                        <form action="{{ route('admin.applications.update', [$application->getRouteKey()]) }}" method="POST" class="d-inline-block">
                            <input type="hidden" name="status" value="{{ \App\Models\ApplicationForm::STATUS_PENDING }}" />
                            @method('PUT')
                            @csrf
                            <button type="submit" class="btn btn-warning btn-big" onclick="return confirm('Are you sure you want mark this as pending? You cannot revert this.')">
                                Pending
                            </button>
                        </form>
                    @endif
                    @if ($application->status == \App\Models\ApplicationForm::STATUS_NEW || $application->status == \App\Models\ApplicationForm::STATUS_PENDING)
                        <form action="{{ route('admin.applications.update', [$application->getRouteKey()]) }}" method="POST" class="d-inline-block">
                            <input type="hidden" name="status" value="{{ \App\Models\ApplicationForm::STATUS_COMPLETED }}" />
                            @method('PUT')
                            @csrf
                            <button type="submit" class="btn btn-success btn-big ml-5" onclick="return confirm('Are you sure you want mark this as completed? You cannot revert this.')">
                                Complete
                            </button>
                        </form>
                        <form action="{{ route('admin.applications.update', [$application->getRouteKey()]) }}" method="POST" class="d-inline-block">
                            <input type="hidden" name="status" value="{{ \App\Models\ApplicationForm::STATUS_REJECTED }}" />
                            @method('PUT')
                            @csrf
                            <button type="submit" class="btn btn-danger btn-big ml-5" onclick="return confirm('Are you sure you want mark this as rejected? You cannot revert this.')">
                                Reject
                            </button>
                        </form>
                    @endif
                </div>
            </div>
    </div>

    @section('footer')
        <script type="text/javascript">
            var app = new Vue({
                el: '#app',
                data: {
                    edit: false,
                    category: {{ $application->category_id }},
                    models: {!! json_encode($application->model) !!},
                    expiry_date:
                        @if (!empty($application->expiry_date))
                            new Date("{{ $application->expiry_date }}".replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"))
                        @else
                            {!! "''" !!}
                        @endif
                    ,
                    last_tested_batch:
                        @if (!empty($application->expiry_date))
                            new Date("{{ $application->last_tested_batch }}".replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"))
                        @else {!! "''" !!}
                        @endif
                    ,
                    date:
                        @if (!empty($application->date))
                            new Date("{{ $application->date }}".replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"))
                        @else {!! "''" !!}
                        @endif
                    ,
                    selected_category: null,
                    categories: {!! json_encode(\App\Models\Category::with(['types'])->where('type', 0)->get()) !!}
                },
                methods: {
                    selectCategory() {
                        this.selected_category = null;
                        if (this.category) {
                            this.categories.forEach((category) => {
                                if (this.category == category.id) {
                                    this.selected_category = category;
                                }
                            });
                        }
                    },
                    toggleEdit() {
                        if (this.edit) {
                            document.getElementById("application-form").reset();
                        }
                        this.edit = !this.edit;
                    }
                },
                mounted() {
                    this.selectCategory();
                },
            })
        </script>
    @endsection
</x-app-layout>
