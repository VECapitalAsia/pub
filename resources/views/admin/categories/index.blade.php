<x-app-layout>
    <div class="container-fluid" id="app">

        @if(session()->has('message'))
        <div class="alert alert-danger">
            {{ session()->get('message') }}
        </div>
        @endif

        {{-- header --}}
        <div class="row justify-content-between align-items-center w-100 border-bottom pb-4">
            <div class="col-6">
                <span>
                    <h5>Category List</h5>
                </span>
            </div>
            <div class="col-6 text-right">
                <a href="{{ route('admin.categories.create') }}" class="btn btn-identity px-5">Create</a>
            </div>
        </div>
        {{-- search and button --}}
        <div class="row my-4">
            <div class="col-lg-4 col-md-6">
                <form id="frmSearch" action="{{ route('admin.categories.index') }}" method="GET">
                    <div class="w-100">
                        <input type="text" name="q" placeholder="Search in the current filter" value="{{ $query }}" class="form-control d-inline-block" style="width: calc(100% - 100px); background: #ececec">
                        <i class="fa fa-search" onclick="document.getElementById('frmSearch').submit()" style="cursor: pointer;"></i>
                    </div>
                </form>
            </div>
        </div>

        {{-- index table --}}
        <div class="row mt-2">
            <div class="table-responsive">
                <table class="table table-rounded table-striped w-100 mt-4">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Types of Products</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($categories as $category)
                            <tr>
                                <td>{{ $category->name }}</td>
                                <td>{{ $category->types()->count() }}</td>
                                <td class="d-flex">
                                    <a class="btn-link btn-sm" href="{{ route('admin.categories.edit', [$category->getRouteKey()]) }}">Edit</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3" class="text-center bg-white">There are no categories.</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        {{ $categories->links() }}
    </div>
</x-app-layout>
