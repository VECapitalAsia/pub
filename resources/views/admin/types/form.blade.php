<x-app-layout>
    <div class="container-fluid" id="app">
        {{-- header --}}
        <div class="d-flex justify-content-between p-3" style="border-bottom-width:2px">
            <div>
                <h4 class="font-weight-bold"> @if (!isset($category))Create @else Edit @endif Product Type</h4>
                <h6>@if (!isset($category))Create @else Edit @endif a new product type</h6>
            </div>
            <div class="mt-3 mb-3" style="border-top-width:3px">
                <div>
                    @if (!empty($category))
                        <form action="{{ route('admin.types.destroy', [$category->getRouteKey()]) }}" method="POST" class="d-inline-block mr-3">
                            @method('DELETE')
                            @csrf
                            <div class="text-right">
                                <button class="btn btn-danger px-4" type="submit" onclick="return confirm('Are you sure you want to delete? This will delete all the types related to this category. You cannot revert this.')">
                                    Delete
                                </button>
                            </div>
                        </form>
                    @endif
                    <a href="{{ route('admin.types.index') }}" class="btn btn-alt px-5">Back</a>
                </div>
            </div>
        </div>

        {{--Form --}}
        <div class="card shadow bg-white mt-4">
            @if(isset($category))
                <form action="{{ route('admin.types.update', $category->getRouteKey()) }}" method="POST" enctype="multipart/form-data">
                @method('PATCH')
            @else
                <form action="{{ route('admin.types.store') }}" method="POST" enctype="multipart/form-data">
            @endif

            @csrf

                <div class="card-header bg-theme rounded-top">
                    <div class="ms-4 mt-2 mb-2">Product Type Information</div>
                </div>

                <div class="card-body">
                    <div class="container mt-5">
                        <div class="form-group row mb-5 mt-5">
                            <label class="col-md-5 text-right col-form-label">Type</label>
                            <div class="col-md-5">
                                <input type="text" name="name" value="{{ !empty(old()) ? old('name') : (isset($category) ? $category->name : '') }}" class="form-control" required />
                                @if($errors->has('name'))
                                <div class="text-danger">{{ $errors->first('name') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-5 mt-5">
                            <label class="col-md-5 text-right col-form-label">Category</label>
                            <div class="col-md-5">
                                <select name="category_id" class="form-control" required>
                                    @foreach ($categories as $categoryTwo)
                                        <option value="{{ $categoryTwo->id }}" {{ !empty($category) ? ($category->category_id == $categoryTwo->id ? 'selected' : '') : '' }}>{{ $categoryTwo->name }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('category_id'))
                                    <div class="text-danger">{{ $errors->first('category_id') }}</div>
                                @endif
                            </div>
                        </div>
                        <input type="hidden" name="type" value="1" />
                        <div class="form-group row mb-5 mt-5">
                            <label class="col-md-5 text-right col-form-label">Position</label>
                            <div class="col-md-5">
                                <input type="number" min="0" name="position" value="{{ !empty(old()) ? old('position') : (isset($category) ? $category->position : '') }}" class="form-control" required />
                                @if($errors->has('category_id'))
                                <div class="text-danger">{{ $errors->first('category_id') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center mt-5 mb-4">
                    <button class="btn btn-primary px-5">{{ isset($category) ? 'Update' : 'Create' }}</button>
                </div>
            </form>
        </div>
    </div>

</x-app-layout>

