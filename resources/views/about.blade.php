@extends('layouts.app')

@section('content')

    <div class="container-fluid p-5" style="background: url('/images/about.png') no-repeat center; background-size: cover; height: 300px;">
    </div>
    <div class="container py-5">
        <div class="row">
            <div class="col-12">
                <h4 style="color: #3D3D3D" class="font-weight-bolder border-bottom border-gray pb-5 mt-4">Fittings & Standard</h4>
                <p class="text-light-gray mt-5">
                    To safeguard public health, compliance to the stipulated standards and requirements is necessary to ensure that the water fittings do not have any adverse impact on water quality, cause water contamination or lead to water wastage and also to ensure their integrity and reliability.
                    <br /><br />
                    The installation and use of the water fittings in potable water service installations shall conform to the Public Utilities (Water Supply) Regulations and Singapore Standard SS 636:2018 (formerly CP48:2005) – Code of Practice for Water Services, As such, every water fitting used for conveyance of piped water for human consumption shall be fit and safe for such purpose.
                    <br /><br />
                    Suppliers, retailers, manufacturers, importers, Professional Engineers, Licensed Plumbers and installers shall ensure that the water fittings comply with every requirement applicable to it as specified in the PUB Stipulation of Standards & Requirements for Water Fittings before they can be offered, displayed or advertised for sale and installation in Singapore.
                    <br /><br />
                    PUB takes a serious view on the safety and reliability of our water supply to consumers and seriousness in water contamination & water leakages to the water supply.
                </p>
                <img src="/images/about-2.png" style="width: 100%" class="my-4" />
                <div class="py-3 border-gray border-bottom py-4">
                    <h6 class="font-weight-bold cursor-pointer" @click="changeStep(2)">Non-Compliance of Water Fittings <span class="float-right font-weight-lighter font-big">@{{ step == 2 ? '+' : '-' }}</span></h6>
                    <p class="mt-5 text-light-gray" v-if="step == 2">
                        It is an offence under the Public Utilities (Water Supply) Regulations to offer for sale, advertise, display, sell or supply or install non-compliant water fittings.
                        <br /><br />
                        All water fittings which are installed by the Licensed Plumbers must comply with PUB’s stipulated requirements and Standards and its use in water service installations conform to the Public Utilities (Water Supply) Regulations and Singapore Standard SS 636:2018 (formerly CP48:2005) – Code of Practice for Water Services.
                        <br /><br />
                        PUB will conduct surveillance inspections and will not hesitate to take action against non-compliance. The penalty for the offence is a fine not exceeding $10,000 or imprisonment for a term not exceeding 12 months or to both
                    </p>
                </div>
                <div class="py-3 border-gray border-bottom py-4">
                    <h6 class="font-weight-bold cursor-pointer" @click="changeStep(3)">PUB Stipulation of Standards & Requirement for Water Fittings <span class="float-right font-weight-lighter font-big">@{{ step == 3 ? '+' : '-' }}</span></h6>
                    <p class="mt-5 text-light-gray" v-if="step == 3">
                        @if ($file = \App\Models\Document::orderBy('updated_at', 'DESC')->first())
                            <a href="{{ $file->url }}" target="_blank" class="text-decoration-underline">
                                The PUB Stipulation of Standards & Requirements for Water Fittings.
                            </a>
                        @endif
                    </p>
                </div>
                <div class="py-3 border-gray border-bottom py-4">
                    <h6 class="font-weight-bold cursor-pointer" @click="changeStep(4)">PUB Circulars <span class="float-right font-weight-lighter font-big">@{{ step == 4 ? '+' : '-' }}</span></h6>
                    <p class="mt-5 text-light-gray" v-if="step == 4">
                        <a href="https://www.pub.gov.sg/compliance/industry/circulars">PUB Circulars link</a>
                    </p>
                </div>
                <div class="mt-4">
                    <p style="color: #666;">For more info, please visit:</p>
                    <a href="https://www.pub.gov.sg/compliance/watersupplyservices/standards" class="text-decoration-underline">https://www.pub.gov.sg/compliance/watersupplyservices/standards</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.14"></script>
    <script type="text/javascript">
        var app = new Vue({
            el: '#app',
            data: {
                step: 1,
            },
            methods: {
                changeStep(step) {
                    if (this.step == step) {
                        this.step = 1;
                    } else {
                        this.step = step;
                    }
                }
            }
        })
    </script>
@endsection
