@extends('layouts.app')

@section('content')

    <div class="container-fluid p-5" style="background: url('/images/apply.png') no-repeat center; background-size: cover; height: 300px;">
    </div>
    <div class="container py-5">
        <div v-if="step == 1">
            <h3 class="font-weight-bolder mt-4">Compliance Notes</h3>
        </div>
        <div v-if="step == 2">
            <h3 class="font-weight-bolder mt-4">Application Form<small class="float-right text-blue cursor-pointer" @click="step--"><i class="fa fa-arrow-left"></i> &nbsp;Back</small></h3>
        </div>
        <div class="container py-3 border-bottom border-gray pb-5 mt-5">
            <div class="row">
                <div class="col-lg-3">
                    <h5 class="font-weight-bold"><span :class="step == 1 ? 'text-blue': ''">Compliance Notes</span> <span class="float-right">&gt;</span></h5>
                </div>
                <div class="col-lg-3">
                    <h5 class="font-weight-bold"><span :class="step == 2 ? 'text-blue': ''">Application Form</span></h5>
                </div>
            </div>
        </div>
        <div class="col-12" v-if="step == 1">
            <p class="text-light-gray mt-5">
                1. PUB takes a serious view on the safety and reliability of our potable water supply to customers and seriousness in water contamination and water leakages to the water supply.
                <br /><br />
                2. Under the Public Utilities (Water Supply) Regulations, every water fitting used for conveyance of piped water for human consumption shall be fit and safe for such purpose.
                <br/><br />
                3. To safeguard public health on the use of water fittings intended for conveyance of water for human consumption, it is MANDATORY to ensure that the water fittings listed in Annex C be tested for compliance with SS 636:2018 and PUB Stipulation of Standards & Requirements for Water Fittings (PUB S&R) under Clause 4 (Annex D) as published in PUB’s website at https://www.pub.gov.sg/compliance/watersupplyservices/standard before the water fittings are allowed for sale, supply and use/installation in Singapore.
                <br/><br />
                4. All water fittings such as pipes, pipe fittings, valves, water storage tanks, taps and mixers, dual-flush low capacity flushing cisterns, flush valves, etc. for use in potable water service installations in Singapore shall be tested to comply with the standards and requirements stipulated by PUB before they can be offered, displayed or advertised for sale and installation in Singapore.
                <br /><br />
                5. Suppliers shall ensure that the water fitting they offer, advertise or supply are supported with complete and valid accredited test reports.
                <br /><br />
                6. Suppliers are to show the applicable valid accredited test reports upon request by Licensed Plumbers, or provide a letter of undertaking to the Licensed Plumbers to show that the water fittings have been tested for compliance with PUB’s requirements.
                <br /><br />
                7. Test reports to be submitted are from testing laboratory accredited by the Singapore Accreditation Council (SAC) or its Mutual Recognition Arrangement (MRA) partners. Test reports issued by a testing laboratory accredited by the SAC or its MRA partners must bear the SAC-SINGLAS mark or the mark of the International Laboratory Accreditation Cooperation Mutual Recognition Arrangement (ILAC-MRA) respectively. For more information, please refer to the PUB S&R which is downloadable from PUB's website https://www.pub.gov.sg/compliance/watersupplyservices/standards
                <br /><br />
                8. Suppliers should ensure that the test reports are properly kept, and must be produced for verification upon request by PUB during PUB's compliance checks, notwithstanding that the test reports or any other supporting documents are submitted to SSWIEA or through SSWIEA's industry partners. Under the Public Utilities Act, PUB can issue notice to any person to furnish information or documents relating to all such matters.
                <br /><br />
                9. Should there be any water quality related issues or any non-compliance with PUB's requirements resulting from the use of such fittings, PUB will investigate and take strict enforcement actions against the responsible parties.
                <br /><br />
                10. For causing water contamination, the penalty will be a fine not exceeding $50,000 or imprisonment for a term not exceeding 3 years or both. The responsible parties will also be required to recall the affected fittings at their own cost and expense.
            </p>
            <div class="text-right mt-3">
                <button type="button" @click="step = 2" class="btn btn-blue btn-big text-white py-2 my-5">Next</button>
            </div>
        </div>

        <div id="step-2" class="row" v-show="step == 2">
            <iframe
                id="JotFormIFrame-213411329915452"
                title="(1/4): Application Form"
                onload="window.parent.scrollTo(0,0)"
                allowtransparency="true"
                allowfullscreen="true"
                allow="geolocation; microphone; camera"
                src="https://form.jotform.com/213411329915452"
                frameborder="0"
                style="
      min-width: 100%;
      height:539px;
      border:none;"
                scrolling="no"
            >
            </iframe>
            <script type="text/javascript">
                var ifr = document.getElementById("JotFormIFrame-213411329915452");
                if (ifr) {
                    var src = ifr.src;
                    var iframeParams = [];
                    if (window.location.href && window.location.href.indexOf("?") > -1) {
                        iframeParams = iframeParams.concat(window.location.href.substr(window.location.href.indexOf("?") + 1).split('&'));
                    }
                    if (src && src.indexOf("?") > -1) {
                        iframeParams = iframeParams.concat(src.substr(src.indexOf("?") + 1).split("&"));
                        src = src.substr(0, src.indexOf("?"))
                    }
                    iframeParams.push("isIframeEmbed=1");
                    ifr.src = src + "?" + iframeParams.join('&');
                }
                window.handleIFrameMessage = function(e) {
                    if (typeof e.data === 'object') { return; }
                    var args = e.data.split(":");
                    if (args.length > 2) { iframe = document.getElementById("JotFormIFrame-" + args[(args.length - 1)]); } else { iframe = document.getElementById("JotFormIFrame"); }
                    if (!iframe) { return; }
                    switch (args[0]) {
                        case "scrollIntoView":
                            iframe.scrollIntoView();
                            break;
                        case "setHeight":
                            iframe.style.height = args[1] + "px";
                            break;
                        case "collapseErrorPage":
                            if (iframe.clientHeight > window.innerHeight) {
                                iframe.style.height = window.innerHeight + "px";
                            }
                            break;
                        case "reloadPage":
                            window.location.reload();
                            break;
                        case "loadScript":
                            if( !window.isPermitted(e.origin, ['jotform.com', 'jotform.pro']) ) { break; }
                            var src = args[1];
                            if (args.length > 3) {
                                src = args[1] + ':' + args[2];
                            }
                            var script = document.createElement('script');
                            script.src = src;
                            script.type = 'text/javascript';
                            document.body.appendChild(script);
                            break;
                        case "exitFullscreen":
                            if      (window.document.exitFullscreen)        window.document.exitFullscreen();
                            else if (window.document.mozCancelFullScreen)   window.document.mozCancelFullScreen();
                            else if (window.document.mozCancelFullscreen)   window.document.mozCancelFullScreen();
                            else if (window.document.webkitExitFullscreen)  window.document.webkitExitFullscreen();
                            else if (window.document.msExitFullscreen)      window.document.msExitFullscreen();
                            break;
                    }
                    var isJotForm = (e.origin.indexOf("jotform") > -1) ? true : false;
                    if(isJotForm && "contentWindow" in iframe && "postMessage" in iframe.contentWindow) {
                        var urls = {"docurl":encodeURIComponent(document.URL),"referrer":encodeURIComponent(document.referrer)};
                        iframe.contentWindow.postMessage(JSON.stringify({"type":"urls","value":urls}), "*");
                    }
                };
                window.isPermitted = function(originUrl, whitelisted_domains) {
                    var url = document.createElement('a');
                    url.href = originUrl;
                    var hostname = url.hostname;
                    var result = false;
                    if( typeof hostname !== 'undefined' ) {
                        whitelisted_domains.forEach(function(element) {
                            if( hostname.slice((-1 * element.length - 1)) === '.'.concat(element) ||  hostname === element ) {
                                result = true;
                            }
                        });
                        return result;
                    }
                };
                if (window.addEventListener) {
                    window.addEventListener("message", handleIFrameMessage, false);
                } else if (window.attachEvent) {
                    window.attachEvent("onmessage", handleIFrameMessage);
                }
            </script>
        </div>
    </div>
@endsection

@section('footer')
    <script type="text/javascript">
        var app = new Vue({
            el: '#app',
            data: {
                category: '',
                models: [{'name': ''}],
                selected_category: '',
                last_tested_date: '',
                submitted_date: '',
                expiry_date: '',
                step: 1,
                categories: {!! json_encode(\App\Models\Category::with(['types'])->where('type', 0)->get()) !!}
            },
            mounted() {
                if (this.categories.length > 0) {
                    this.category = this.categories[0].id;
                    this.selectCategory();
                }
            },
            methods: {
                selectCategory() {
                    this.selected_category = null;
                    if (this.category) {
                        this.categories.forEach((category) => {
                           if (this.category == category.id) {
                               this.selected_category = category;
                           }
                        });
                    }
                },
                changeStep(step) {
                    if (step > 1) {
                        var result = $("#step-" + this.step + " input").filter(function () {
                            return $.trim($(this).val()).length == 0
                        }).length === 0;
                        if (!result) {
                            alert('Please fill up all fields before proceeding.');
                            return;
                        }
                        if (step == 3) {
                            if ($('#contact_number').val().trim().replace(/ /g,'').length != 8) {
                                alert('Please make sure your contact number is 8 characters.');
                                return;
                            } else if (!(/^\d+$/.test($('#contact_number').val().trim().replace(/ /g,'')))) {
                                alert('Please make sure your contact number is only digits.');
                                return;
                            }
                        }
                    }
                    this.step = step;
                },
            }
        })
    </script>
@endsection
