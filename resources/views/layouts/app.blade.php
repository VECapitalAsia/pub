<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body class="frontpage">
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container-fluid py-3">
                <a class="navbar-brand mx-xl-5 px-xl-5" href="/">
                    <img src="/images/logo.svg?v=1" height="70">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                        <li class="nav-item {{ request()->routeIs('index') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('index') }}">Home</a>
                        </li>

                        <li class="nav-item {{ request()->routeIs('about') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('about') }}">About</a>
                        </li>

                        <li class="nav-item {{ request()->routeIs('products') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('products') }}">Registered Product</a>
                        </li>

                        <li class="nav-item {{ request()->routeIs('apply') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('apply') }}">Apply Here</a>
                        </li>

                        <li class="nav-item {{ request()->routeIs('contact') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('contact') }}">Contact Us</a>
                        </li>

                    </ul>

            </div>
        </nav>

        <main>
            @yield('content')
        </main>

        <footer class="container-fluid bg-footer px-5 py-3 pt-5 position-relative bg-white shadow-lg">
            <div class="container pb-3" style="font-size: 16px;">
                <div class="row">
                    <div class="col-lg-4 pe-5">
                        <img src="/images/logo-footer.png" height="180px" class="mw-100 pl-2" style="object-fit: contain" />

                        <a href="https://www.sswiea.org.sg" target="_blank"><img src="/images/group-logo-2.jpg" class="mw-100" width="150px" /></a>
                    </div>
                    <div class="col-lg-4 mt-5 mt-lg-0">
                        <h6 class="text-muted mb-3 font-weight-bolder">Contact Us</h6>
                        <p class="mt-2">Tel : + 65 6977 9848</p>
                        <p class="mt-2">Email : <a href="mailto:info@waterfittings.sg">info@waterfittings.sg</a></p>
                    </div>
                    <div class="col-lg-4 mt-5 mt-lg-0">
                        <h6 class="text-muted mb-3 font-weight-bolder">Address</h6>
                        <span class="fs-6 fw-light mt-2">
                            Administration Office/Mailing:<br /><br />
                            18 Sin Ming Lane, #02-08, Midview City,<br />
                            Singapore 573960<br /><br />
                            Association's HQ:<br /><br />
                            61 Ubi Road 1 #02-02, Oxley Bizhub,<br />
                            Singapore 408727
                        </span>
                    </div>
                </div>
            </div>

        </footer>
        <div class="container-fluid" style="background: #67B1EA">
            <div class="row text-white py-2">
                <div class="col-lg-6 fw-light">
                    <small>Copyright &copy; 2021 SSWIEA</small>
                </div>
            </div>
        </div>
    </div>
    @yield('footer')
</body>
</html>
