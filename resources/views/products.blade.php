@extends('layouts.app')

@section('content')
    <div class="container-fluid p-5" style="min-height: calc(100vh - 495px);">
        <div class="row">
            <div class="col-12">
                <h4 style="color: #3D3D3D" class="font-weight-bolder text-black pb-5 mt-4">Registered Product Listing</h4>
                <div class="col-xl-6">
                    <form method="GET" action="{{ route('products') }}">
                        <input type="text" name="search" class="form-control" value="{{ request('search', '') }}" placeholder="Search.." />
                    </form>
                </div>
            </div>
        </div>
        <div class="mt-5 col-12">
            <div class="row">
                @foreach ($categories as $category)
                    <div class="col-lg-4 col-md-6">
                        <div class="card shadow-sm border-grayish mb-5">
                            <div class="card-header bg-white">
                                <h6 class="text-blue font-weight-bolder" style="font-size: 18px;">{{ $category->name }}</h6>
                            </div>
                            <div class="card-body card-body-scroll">
                                @foreach ($category->categories as $type)
                                    <a href="{{ route('categories.products', [$type->getRouteKey()]) }}" class="type-link d-block mb-3" style="font-size: 18px; color: #707070;">{{ $type->name }}<span class="float-right"><i class="fa fa-angle-right" style="color:  #3D1FA6;"></i></span></a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="mt-5">
            <p class="text-light-gray mt-5">
                For registration requirements and list of WELS products, please refer to PUB's WELS website at<br />
                <a href="https://www.pub.gov.sg/wels/suppliers">https://www.pub.gov.sg/wels/suppliers</a> and <a href="https://www.pub.gov.sg/wels/welsproducts">https://www.pub.gov.sg/wels/welsproducts</a> respectively.
            </p>
        </div>
    </div>
@endsection
