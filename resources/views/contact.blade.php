@extends('layouts.app')

@section('content')

    <div class="container-fluid p-5" style="background: url('/images/contact.png') no-repeat center; background-size: cover; min-height: calc(100vh - 495px)">
        <form id="contact-form" method="POST" action="{{ route('contact') }}">
            @csrf

            <div class="container p-5">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2" >
                        <h1 class="text-center font-weight-bolder text-white">Contact Us</h1>
                        @if ($message = session('message'))
                            <div class="container py-2">
                                <div class="text-center my-5 bg-success rounded p-3 text-white">
                                    {{ $message }}
                                </div>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-lg-6 mt-3">
                                <input type="text" name="first_name" placeholder="First Name" class="form-control" required />
                            </div>
                            <div class="col-lg-6 mt-3">
                                <input type="text" name="last_name" placeholder="Last Name" class="form-control" required />
                            </div>
                            <div class="col-12 mt-3">
                                <input type="email" name="email" placeholder="Email" class="form-control" required />
                            </div>
                            <div class="col-12 mt-3">
                                <textarea name="message" placeholder="Message" class="form-control" required></textarea>
                            </div>
                            <div class="col-12 mt-3">
                                <button type="submit" class="btn btn-primary w-100">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
