@extends('layouts.app')

@section('content')
    <img src="/images/home.png" style="width: 100%" />
    <div class="container py-5">
        <div class="row">
            <div class="col-lg-6 d-flex align-items-center">
                <h3 class="font-weight-bolder fs-35">WATER FITTINGS REGISTRATION AND INFORMATION SYSTEM<br /> [For Use in Water Service Installations]<br />
                    <a href="{{ route('apply') }}" class="btn btn-lblue btn-big text-white py-3 my-5">Apply Now</a></h3>

            </div>
            <div class="col-lg-6">
                <div class="card shadow-sm border-0">
                    <div class="card-body p-5">
                        <h3 class="text-center mb-5 text-blue font-weight-bold fs-35">A Joint Programme by</h3>
                        <div class="row">
                            <div class="col-7 align-items-center d-flex">
                                <a href="https://www.pub.gov.sg" target="_blank"><img src="/images/group-logo-1.jpg" class="w-100" /></a>
                            </div>
                            <div class="col-5">
                                <a href="https://www.sswiea.org.sg" target="_blank"><img src="/images/group-logo-2.jpg" class="w-100" /></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mt-4">
            <p class="mt-4">
                Water fittings such as pipes, pipe fittings, valves, taps/mixers, urinal flush valves, flush valves for water closets (WCs), dual-flush low capacity flushing cisterns (LCFCs), coating/lining materials in contact with potable water, water storage tanks and other products as stipulated by PUB from time to time, <b>SHALL COMPLY</b> with the standards and requirements stipulated by PUB before they can be offered-for-sale, displayed, advertised, supply or installed in potable water service installations<br /><br />
                <span class="text-danger"><b>It is an offence under the Public Utilities (Water Supply) Regulations to offer for sale, advertise, display, sell or supply or install non- compliant water fittings.</b></span>
                <br /><br />
                The WATER FITTINGS REGISTRATION AND INFORMATION SYSTEM [For Use in Water Service Installations] is a newly launched programme by SSWIEA in late 2021.This registration system allows SSWIEA to assist and facilitate in the checking for compliance of water fittings to ensure that these products are reliable/durable and do not have any adverse impact on water quality, cause water contamination or water wastage.
                <br /><br />
                The list of products is for industry’s reference, and should not be in any way deemed as products being approved by PUB.
                <br /><br />
                For more details on PUB’s requirements on water fittings, please refer PUB’s website at <a href="https://www.pub.gov.sg/compliance/watersupplyservices/standards">https://www.pub.gov.sg/compliance/watersupplyservices/standards</a>.
                <br />
            </p>
            <div class="text-center my-5">
                <h5>Have your products registered with us soon.</h5>

                <a href="{{ route('apply') }}" class="btn btn-blue btn-big text-white py-3 my-5">Click to Register</a>
            </div>
        </div>
    </div>
@endsection
