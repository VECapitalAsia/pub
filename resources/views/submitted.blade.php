@extends('layouts.app')

@section('content')

    <div class="container-fluid p-5" style="background: url('/images/apply.png') no-repeat center; background-size: cover; height: 300px;">
    </div>
    <div class="container py-5" style="min-height: 525px;">
        <h3 class="font-weight-bolder mt-4">Application</h3>
        <div class="border-bottom border-gray">
            <h5 class="font-weight-bold mt-5">Thank you!</h5>
            <p class="font-weight-bold mt-3">
                Your submission has been recorded. Reference ID: {{ $form->id }}<br/><br />
                You will be notified on your application in a separate email.
            </p>
        </div>
        <div class="text-right pt-3">
            <h5><a href="{{ route('index') }}">Homepage</a></h5>
        </div>
    </div>
@endsection

@section('footer')
    <script type="text/javascript">
        var app = new Vue({
            el: '#app',
            data: {
                last_tested_date: '',
                submitted_date: '',
                step: 4,
            },
        })
    </script>
@endsection
