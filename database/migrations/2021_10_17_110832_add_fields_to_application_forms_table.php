<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToApplicationFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('application_forms', function (Blueprint $table) {
            $table->string('cert_no')->nullable()->after('manufacturer');
            $table->string('company')->nullable()->after('contact_number');
            $table->json('model')->nullable()->change();
            $table->date('expiry_date')->nullable()->after('date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('application_forms', function (Blueprint $table) {
            $table->dropColumn('cert_no');
            $table->dropColumn('company');
            $table->string('model')->nullable()->change();
            $table->dropColumn('expiry_date');
        });
    }
}
