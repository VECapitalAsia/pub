<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_forms', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('uen')->nullable();
            $table->string('office_address')->nullable();
            $table->string('office_email')->nullable();
            $table->string('contact_person')->nullable();
            $table->string('designation')->nullable();
            $table->string('email_address')->nullable();
            $table->string('contact_number')->nullable();
            $table->foreignId('category_id')->nullable()->constrained();
            $table->foreignId('type_of_product_id')->nullable()->constrained('categories', 'id');
            $table->string('type_of_materials')->nullable();
            $table->string('country')->nullable();
            $table->string('brand')->nullable();
            $table->string('model')->nullable();
            $table->string('size')->nullable();
            $table->string('manufacturer')->nullable();
            $table->date('last_tested_batch')->nullable();
            $table->text('remarks')->nullable();
            $table->date('date')->nullable();
            $table->string('name_submitted')->nullable();
            $table->string('designation_submitted')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_forms');
    }
}
