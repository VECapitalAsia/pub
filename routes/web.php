<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('index');

Route::get('/about', function () {
    return view('about');
})->name('about');
Route::get('/products', [App\Http\Controllers\HomeController::class, 'products'])->name('products');
Route::get('/categories/{category}/products', [App\Http\Controllers\HomeController::class, 'categoryShow'])->name('categories.products');
Route::get('/apply', [App\Http\Controllers\HomeController::class, 'apply'])->name('apply');
Route::post('/apply', [App\Http\Controllers\HomeController::class, 'applySubmit']);
Route::get('/contact', [App\Http\Controllers\HomeController::class, 'contact'])->name('contact');
Route::post('/contact', [App\Http\Controllers\HomeController::class, 'contactSubmit']);

Auth::routes(['register' => false]);

Route::get('logout', [\App\Http\Controllers\Auth\LoginController::class, 'logout']);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['auth']], function() {


    Route::get('/', [\App\Http\Controllers\HomeController::class, 'admin'])->name('dashboard');

    Route::resource('categories', App\Http\Controllers\Admin\CategoryController::class);
    Route::resource('types', App\Http\Controllers\Admin\TypeController::class);
    Route::get('products/export', [\App\Http\Controllers\Admin\ProductController::class, 'export'])->name('products.export');
    Route::post('products/import', [\App\Http\Controllers\Admin\ProductController::class, 'import'])->name('products.import');
    Route::resource('products', App\Http\Controllers\Admin\ProductController::class);
    Route::resource('applications', App\Http\Controllers\Admin\ApplicationFormController::class);
    Route::resource('documents', App\Http\Controllers\Admin\DocumentController::class);

});
